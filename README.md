# Visual Studio Marketplace #
[![Marketplace Version](https://img.shields.io/visual-studio-marketplace/v/marcinbar.vscode-squirrel.svg)](https://marketplace.visualstudio.com/items?itemName=marcinbar.vscode-squirrel)
[![Installs](https://img.shields.io/visual-studio-marketplace/i/marcinbar.vscode-squirrel.svg)](https://marketplace.visualstudio.com/items?itemName=marcinbar.vscode-squirrel)
[![Rating](https://img.shields.io/visual-studio-marketplace/r/marcinbar.vscode-squirrel.svg)](https://marketplace.visualstudio.com/items?itemName=marcinbar.vscode-squirrel)

# Open VSX
[![Version](https://img.shields.io/open-vsx/v/marcinbar/vscode-squirrel)](https://open-vsx.org/extension/marcinbar/vscode-squirrel)
[![Installs](https://img.shields.io/open-vsx/dt/marcinbar/vscode-squirrel)](https://open-vsx.org/extension/marcinbar/vscode-squirrel)
[![Rating](https://img.shields.io/open-vsx/rating/marcinbar/vscode-squirrel)](https://open-vsx.org/extension/marcinbar/vscode-squirrel)

# Squirrel Language Supports #
Syntax highlighing, completions and formatting support for squirrel language


# Features
* Syntax highlighting and bracket matching.
![highlighting](https://bitbucket.org/marcinbar91/vscode-squirrel/raw/79bf26583a59a30c60e899e89136e48e1c5899c8/images/highlighting.gif)

* Completions.
![completion](https://bitbucket.org/marcinbar91/vscode-squirrel/raw/79bf26583a59a30c60e899e89136e48e1c5899c8/images/completion.gif)

* JSDoc Completions.
![jsdoccompletion](https://bitbucket.org/marcinbar91/vscode-squirrel/raw/79bf26583a59a30c60e899e89136e48e1c5899c8/images/jsdoccompletion.gif)

* Formatting.
![formatting](https://bitbucket.org/marcinbar91/vscode-squirrel/raw/79bf26583a59a30c60e899e89136e48e1c5899c8/images/formatting.gif)

* Hover.
![hover](https://bitbucket.org/marcinbar91/vscode-squirrel/raw/79bf26583a59a30c60e899e89136e48e1c5899c8/images/hover.gif)

* Signature Help.
![signature](https://bitbucket.org/marcinbar91/vscode-squirrel/raw/79bf26583a59a30c60e899e89136e48e1c5899c8/images/signature.gif)

* Breadcrumb.
![bread](https://bitbucket.org/marcinbar91/vscode-squirrel/raw/79bf26583a59a30c60e899e89136e48e1c5899c8/images/bread.gif)

* Inlay Hints.
* OnTypeFormatting. (must be checked editor.formatOnType in settings)

# License
You are free to use this in any way you want, in case you find this useful or working for you but you must keep the copyright notice and license. (MIT)


# Credits
* JS Beautifier <https://github.com/beautify-web/js-beautify/>
* Complete JSDoc Tags <https://github.com/HookyQR/VSCodeJSDocTagComplete>
* ivchups for a some fixes <https://bitbucket.org/ivchups/vscode-squirrel_iv/commits/0ce3ac637b61da6bb601d8fbe6203a7c8e72fe5d>
* (S)quirrel for a syntaxes <https://github.com/mepsoid/vscode-s-quirrel>
* JSDoc syntax highlighting for a jsdoc syntax <https://github.com/galloween/jsdoc-highlight-code>
