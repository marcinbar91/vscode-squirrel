const vscode = require('vscode');
const beautify = require('../js-beautify');
const configuration = vscode.workspace.getConfiguration('vscode-squirrel', undefined)

class provideOnTypeFormattingEdits extends vscode.TextEdit {
    /**
    * @param {vscode.TextDocument} document
    * @param {vscode.Position} position
    * @param {string} ch
    * @param {vscode.FormattingOptions} options
    * @param {vscode.CancellationToken} token
    * @returns {vscode.TextEdit[]}
    */
    provideOnTypeFormattingEdits(document, position, ch, options, token) {
        return new Promise((resolve, reject) => {
            const fullText = document.lineAt(position.line).text
            const fullRange = new vscode.Range(position.line, 0, position.line, fullText.length);
            resolve([vscode.TextEdit.replace(fullRange, beautify.js_beautify(fullText, { "indent_with_tabs": configuration.get('IndentWithTabs') }).replace(/local\s+function/g, "local function").replace("< -", "<- "))]);
        });
    }
}

module.exports = { provideOnTypeFormattingEdits }